# Cat-Dog Image Classifier
cat dog image classifier implemented into website using flask

To run:
-
Create virtual environment using following command.

- virtualenv venv

Install requirement using following command.
- pip install -r requirements.txt


Clone or Download using following link.


Open directory in cmd and type run following command.
 - app.py

Open following URL in browser.
- http://127.0.0.1:5000/